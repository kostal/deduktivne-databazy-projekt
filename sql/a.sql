
DROP DATABASE IF EXISTS `population`;
CREATE DATABASE population;
USE population;

source places.sql;

DROP TABLE IF EXISTS `names_men`;
CREATE TABlE names_men (
    name VARCHAR(200)
);
INSERT INTO names_men (name) VALUES ("Peter"),("Milan"),("Jakub"),("Juraj"),("Pavol"),("Ondrej"),("Oliver"),("Martin"),("Jozef"),("Ján"),("Andrej"),("Adam"),("Patrik");

DROP TABLE IF EXISTS `names_women`;
CREATE TABlE names_women (
    name VARCHAR(200)
);
INSERT INTO names_women (name) VALUES ("Katarína"),("Žaneta"),("Petra"),("Martina"),("Viera"),("Jaroslava"),("Zuzana"),("Lucia"),("Jarmila"),("Erika"),("Anna"),("Ľudmila");

DROP TABLE IF EXISTS `surnames`;
CREATE TABlE surnames (
    name VARCHAR(200)
);
INSERT INTO surnames (name) VALUES ("Slovák"),("Kučer"),("Košťál"),("Horvath"),("Petrík"),("Simčák"),("Hudák"),("Berger"),("Dolák"),("Rusnák"),("Bartík"),("Mlynarovič");

DELIMITER //

CREATE FUNCTION randomRowNameMen() RETURNS VARCHAR(200)
    DETERMINISTIC
BEGIN
    DECLARE count int;
    declare randomNum int;
    declare row varchar(200);
    declare sum int;
    DECLARE cur CURSOR FOR SELECT name FROM names_men;

    set count = (SELECT COUNT(*) FROM names_men);
    set randomNum =  round(RAND() * (count - 0) + 0);
    set sum =0;
    open cur;
    loop1: loop
        If sum = randomNum THEN
            leave loop1;
        end if;
        FETCH cur INTO row;
        set sum = sum+1;
    END LOOP loop1;
 RETURN row;
END;
//


DELIMITER //

CREATE FUNCTION randomRowNameWomen() RETURNS VARCHAR(200)
    DETERMINISTIC
BEGIN
    DECLARE count int;
    declare randomNum int;
    declare row varchar(200);
    declare sum int;
    DECLARE cur CURSOR FOR SELECT name FROM names_women;

    set count = (SELECT COUNT(*) FROM names_women);
    set randomNum =  round(RAND() * (count - 0) + 0);
    set sum =0;
    open cur;
    loop1: loop
        If sum = randomNum THEN
            leave loop1;
        end if;
        FETCH cur INTO row;
        set sum = sum+1;
    END LOOP loop1;
 RETURN row;
END;
//

DELIMITER //

CREATE FUNCTION randomSurname() RETURNS VARCHAR(200)
    DETERMINISTIC
BEGIN
    DECLARE count int;
    declare randomNum int;
    declare row varchar(200);
    declare sum int;
    DECLARE cur CURSOR FOR SELECT name FROM surnames;

    set count = (SELECT COUNT(*) FROM surnames);
    set randomNum =  round(RAND() * (count - 0) + 0);
    set sum =0;
    open cur;
    loop1: loop
        If sum = randomNum THEN
            leave loop1;
        end if;
        FETCH cur INTO row;
        set sum = sum+1;
    END LOOP loop1;
 RETURN row;
END;
//

DELIMITER //

CREATE FUNCTION randomAdress() RETURNS VARCHAR(200)
    DETERMINISTIC
BEGIN
    DECLARE count int;
    declare randomNum int;
    declare row varchar(200);
    declare sum int;
    DECLARE cur CURSOR FOR SELECT name FROM places;

    set count = (SELECT COUNT(*) FROM places);
    set randomNum =  round(RAND() * (count - 0) + 0);
    set sum =0;
    open cur;
    loop1: loop
        If sum = randomNum THEN
            leave loop1;
        end if;
        FETCH cur INTO row;
        set sum = sum+1;
    END LOOP loop1;
 RETURN row;
END;
//

DELIMITER //

CREATE PROCEDURE add_person ()

BEGIN
    DECLARE  sex INT;
    DECLARE  nameMenCount INT;
    DECLARE nameMenRandom INT;
    DECLARE name VARCHAR(200);
    DECLARE surname VARCHAR(200);
    declare birthdate date;
    declare height int;
    declare weight int;
    declare adress varchar(200);

    set sex = floor(RAND() *2 );
    IF sex = 1 THEN
        set name = randomRowNameWomen();
        set surname = randomSurname();
        set surname = CONCAT(surname,"ová");
        set height = floor(RAND() *55 + 150 );
        set weight = floor(RAND() *150 + 50 );
    ELSE
        set name = randomRowNameMen();
        set surname = randomSurname();
        set height = floor(RAND() *60 + 160 );
        set weight = floor(RAND() *100 + 30 );
    END IF;
    set birthdate =  DATE(DATE_SUB(NOW(), INTERVAL ROUND(RAND()*365*100) DAY)) ;
    set adress = randomAdress();

    insert into population (sex,name,surname,birthdate,height,weight,adress) values (sex,name,surname,birthdate,height,weight,adress);
    -- SELECT  name,surname,birthdate,height,adress;
END;
//

DELIMITER //

CREATE PROCEDURE addPopulation (n int)

BEGIN
    declare x INT default 0;
    DROP TABLE IF EXISTS `population`;
    CREATE TABlE population (
        id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
        sex TINYINT(1),
        name VARCHAR(200),
        surname VARCHAR(200),
        birthdate date,
        height int,
        weight int,
        adress VARCHAR (200)
    );
    WHILE x <= n DO
        call add_person();
        SET x = x + 1;
  END WHILE;
  call createRelationships(n);
END;
//

DELIMITER //

CREATE PROCEDURE createRelationships (n int)

BEGIN
    DROP TABLE IF EXISTS `person2person`;
    CREATE TABlE person2person (
        id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
        person1Id int NOT NULL,
        person2Id int NOT NULL
    );
    insert into person2person (person1Id,person2Id) select p1.id, p2.id from population as p1 cross join population as p2 where rand() <0.05;

END;
//


DELIMITER //

CREATE PROCEDURE getPopulationLimit (n int)

BEGIN
    select * from population limit n;
END;
//

DELIMITER //

CREATE PROCEDURE getFriendsRelationships (n int)

BEGIN
    SELECT * FROM population WHERE id IN (
    SELECT person1Id FROM person2person WHERE person2Id = n
    UNION
    SELECT person2Id FROM person2person WHERE person1Id = n
    );
END;
//

DELIMITER //

CREATE PROCEDURE getPerson2Person ()

BEGIN
    SELECT * FROM person2person;
END;
//

DELIMITER ;

call addPopulation(100);
call getPopulationLimit(20);
call getPerson2Person();
call getFriendsRelationships(1);
