Map.prototype.codeAddress = function(addressString,person) {
	var self = this;
	var address = addressString.trim();
	this.geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == 'OK') {
			person.position = results[0].geometry.location;
			var marker = new google.maps.Marker({
				map: self.map,
				position: results[0].geometry.location,
				icon: "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 278.16 384.51' height='35px' width='25px'><defs><clipPath id='a' transform='translate(0.01 -0.01)'><rect x='-0.01' width='278.16' height='384.51' style='fill:none'/></clipPath></defs><title>wswidd-iconmarker1</title><g style='clip-path:url(%23a)'><path d='M139.07,384.51S278.15,215.93,278.15,139.09A139.08,139.08,0,0,0,0,139.09C0,215.9,139.07,384.52,139.07,384.52' transform='translate(0.01 -0.01)' style='fill:%23e2001a'/><path d='M139.07,231.36a92.27,92.27,0,1,0-92.29-92.27,92.27,92.27,0,0,0,92.29,92.27h0' transform='translate(0.01 -0.01)' style='fill:%23fff'/></g><text transform='translate(137.5 170.66)' text-anchor='middle' style='font-size:100px;fill:%23e2001a;font-family:Arial'>"+person.id.toString()+"</text></svg>"

			});
			var content = self.addContentToInfoWindow(addressString,person);
		    var infowindow = new google.maps.InfoWindow();
		    infowindow.setContent(content);
		    google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
		            return function() {
		                if(self.selectedInfoWindow!=null) self.selectedInfoWindow.close();
		                self.selectedInfoWindow=infowindow;
		                infowindow.open(this.map,marker);
		                };
		            })(marker,content,infowindow))
		    return marker;
		}
		else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
			setTimeout(function() {
				self.codeAddress(address,person);
			}, 200);
		}
		else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
			var ar = address.split(" ");
			self.codeAddress(ar[0]+" "+ar[1],person);
		} else {
			alert('Geocode was not successful for the following reason: ' + status + address);
		}
	});
}

Map.prototype.addContentToInfoWindow = function(address,person){
    var content ="<div>";
	content+="<p>id:"+person.id+"</p>";
	content+="<p>"+person.name+"  "+person.surname+"</p>";
	content+="<p>"+person.birthdate+"</p>";
	content+="<p>"+address+"</p>";
	content+="<p>Vyska:"+person.height+"</p>";
	content+="<p>Vaha:"+person.weight+"</p>";
    content+="</div>";
    return content
}

Map.prototype.getPerson = function(id){
	for( var i =0;i<people.length;i++){
		if(people[i].id == id){
			return people[i];
		}
	}
}

Map.prototype.addPeople = function(callback){
	for( var i=0; i<people.length; i++){
		var adress = people[i].adress;
		adress = adress.substring(0,adress.length-4);
		this.codeAddress(adress,people[i]);
	}
	callback();
}

function Map(mapSelector){
	this.mapSelector = mapSelector;
	this.geocoder = new google.maps.Geocoder();
	this.selectedInfoWindow = null;
}

Map.prototype.initMap = function(center,zoom){
    var mapDiv = $(this.mapSelector)[0];
    this.map = new google.maps.Map(mapDiv, {
        center: center,
        zoom: zoom,
        disableDefaultUI: true,
		zoomControl: true,

    });
}

window.onload =  function() {
	map = new Map(".map");
	map.initMap({lat:48.759873, lng:19.648323},8,true);
	map.addPeople(function(){

	});
	// $(".btn").click(function(){
	// 	map.drawRelationships();
	// });
}
