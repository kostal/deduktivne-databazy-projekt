// tato cast sluzi na inicializaciu a vykreslenie markerov na strane klienta

function Map(mapSelector){
	this.mapSelector = mapSelector;
	this.geocoder = new google.maps.Geocoder();
	this.selectedInfoWindow = null;
}

//vytvori zakladnu mapu
Map.prototype.initMap = function(center,zoom){
    var mapDiv = $(this.mapSelector)[0];
    this.map = new google.maps.Map(mapDiv, {
        center: center,
        zoom: zoom,
        disableDefaultUI: true,
		zoomControl: true,

    });
}

//prida infowindow k markeru
Map.prototype.addContentToInfoWindow = function(address,person){
    var content ="<div>";
	content+="<p>index:"+person.index+"</p>";
	content+="<p>"+person.name+"  "+person.surname+"</p>";
	content+="<p>"+person.birthdate+"</p>";
	content+="<p>"+address+"</p>";
	content+="<p>Vyska:"+person.weight+"</p>";
	content+="<p>Vaha:"+person.height+"</p>";
	content+="<p>"+person.family_indexes.toString()+"</p>";
    content+="</div>";
    return content
}

//najde na mape pozadovanu adresu
Map.prototype.codeAddress = function(addressString,person) {
	var self = this;
	var address = addressString.trim();
	this.geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == 'OK') {
			person.position = results[0].geometry.location;
			var marker = new google.maps.Marker({
				map: self.map,
				position: results[0].geometry.location,
				icon: "data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 278.16 384.51' height='35px' width='25px'><defs><clipPath id='a' transform='translate(0.01 -0.01)'><rect x='-0.01' width='278.16' height='384.51' style='fill:none'/></clipPath></defs><title>wswidd-iconmarker1</title><g style='clip-path:url(%23a)'><path d='M139.07,384.51S278.15,215.93,278.15,139.09A139.08,139.08,0,0,0,0,139.09C0,215.9,139.07,384.52,139.07,384.52' transform='translate(0.01 -0.01)' style='fill:%23e2001a'/><path d='M139.07,231.36a92.27,92.27,0,1,0-92.29-92.27,92.27,92.27,0,0,0,92.29,92.27h0' transform='translate(0.01 -0.01)' style='fill:%23fff'/></g><text transform='translate(137.5 170.66)' text-anchor='middle' style='font-size:100px;fill:%23e2001a;font-family:Arial'>"+person.index.toString()+"</text></svg>"

			});
			var content = self.addContentToInfoWindow(addressString,person);
		    var infowindow = new google.maps.InfoWindow();
		    infowindow.setContent(content);
		    google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
		            return function() {
		                if(self.selectedInfoWindow!=null) self.selectedInfoWindow.close();
		                self.selectedInfoWindow=infowindow;
		                infowindow.open(this.map,marker);
		                };
		            })(marker,content,infowindow))
		    return marker;
		}
		else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
			setTimeout(function() {
				self.codeAddress(address,person);
			}, 200);
		}
		else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
			var ar = address.split(" ");
			self.codeAddress(ar[0]+" "+ar[1],person);
		} else {
			alert('Geocode was not successful for the following reason: ' + status + address);
		}
	});
}

//nakrsli vztahy na mapke
Map.prototype.drawRelationships = function(){
	for(var i in relationships){
		if(relationships[i].size>1){
			var ar = [];
			for(var j=0;j<relationships[i].people.length;j++){
				var index = relationships[i].people[j].index;
				person = this.getPerson(index);
				console.log(index);
				console.log(person);
				ar.push(person.position);
			}

			var path = new google.maps.Polyline({
				path: ar,
				geodesic: true,
				strokeColor: this.getRandColor(3),
				strokeOpacity: 1.0,
				strokeWeight: 3
			});

			path.setMap(this.map);


		}
	}
}

Map.prototype.getRandColor = function(brightness){
    // Six levels of brightness from 0 to 5, 0 being the darkest
    var rgb = [Math.random() * 256, Math.random() * 256, Math.random() * 256];
    var mix = [brightness*51, brightness*51, brightness*51]; //51 => 255/5
    var mixedrgb = [rgb[0] + mix[0], rgb[1] + mix[1], rgb[2] + mix[2]].map(function(x){ return Math.round(x/2.0)})
    return "rgb(" + mixedrgb.join(",") + ")";
}

Map.prototype.getPerson = function(index){
	for( var i in people){
		if(people[i].index == index){
			return people[i];
		}
	}
}

//prida ludi s polohou(markre) na mapu
Map.prototype.addPeople = function(callback){
	for( var i in people){
		var address = Object.values(people[i].address)[1];
		address = address.substring(0,address.length-4);
		this.codeAddress(address,people[i]);
	}
	callback();
}

//main
window.onload =  function() {
	map = new Map(".map");
	map.initMap({lat:48.759873, lng:19.648323},8,true);
	map.addPeople(function(){

	});
	$(".btn").click(function(){
		map.drawRelationships();
	});
}
