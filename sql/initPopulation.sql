
--DROP DATABASE IF EXISTS population;
--CREATE DATABASE population;
\c population;

--source places.sql;

DROP TABLE IF EXISTS people;
CREATE TABlE people (
    id serial primary key,
    sex int,
    name VARCHAR(200),
    surname VARCHAR(200),
    birthdate date,
    height int,
    weight int,
    adress VARCHAR (200)
);

DROP TABLE IF EXISTS names_men;
CREATE TABlE names_men (
    name VARCHAR(200)
);
INSERT INTO names_men (name) VALUES ('Peter'),('Milan'),('Jakub'),('Juraj'),('Pavol'),('Ondrej'),('Oliver'),('Martin'),('Jozef'),('Ján'),('Andrej'),('Adam'),('Patrik');

DROP TABLE IF EXISTS names_women;
CREATE TABlE names_women (
    name VARCHAR(200)
);
INSERT INTO names_women (name) VALUES ('Katarína'),('Žaneta'),('Petra'),('Martina'),('Viera'),('Jaroslava'),('Zuzana'),('Lucia'),('Jarmila'),('Erika'),('Anna'),('Ľudmila');

DROP TABLE IF EXISTS surnames;
CREATE TABlE surnames (
    name VARCHAR(200)
);
INSERT INTO surnames (name) VALUES ('Slovák'),('Kučer'),('Košťál'),('Horvath'),('Petrík'),('Simčák'),('Hudák'),('Berger'),('Dolák'),('Rusnák'),('Bartík'),('Mlynarovič');

CREATE OR REPLACE FUNCTION randomRowNameMen ()
RETURNS varchar(200) AS $row$
declare
    count int;
    randomNum int;
    row varchar(200);
    sum int;
    cur CURSOR FOR SELECT name FROM names_men;
BEGIN
    count := (SELECT COUNT(*) FROM names_men);
    randomNum :=  floor(random() * (count - 0) + 0);
    sum :=0;
    open cur;
    FOR i IN 0..count LOOP
        if sum = randomNum THEN
            close cur;
            exit;
        end if;
        FETCH cur INTO row;
        sum := sum+1;
    END LOOP;
 RETURN row;
END;
$row$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION randomRowNameWomen ()
RETURNS varchar(200) AS $row$
declare
    count int;
    randomNum int;
    row varchar(200);
    sum int;
    cur CURSOR FOR SELECT name FROM names_women;
BEGIN
    count := (SELECT COUNT(*) FROM names_women);
    randomNum :=  floor(random() * (count - 0) + 0);
    sum :=0;
    open cur;
    FOR i IN 0..count LOOP
        if sum = randomNum THEN
            close cur;
            exit;
        end if;
        FETCH cur INTO row;
        sum := sum+1;
    END LOOP;
 RETURN row;
END;
$row$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION randomSurname ()
RETURNS varchar(200) AS $row$
declare
    count int;
    randomNum int;
    row varchar(200);
    sum int;
    cur CURSOR FOR SELECT name FROM surnames;
BEGIN
    count := (SELECT COUNT(*) FROM surnames);
    randomNum :=  floor(random() * (count - 0) + 0);
    sum :=0;
    open cur;
    FOR i IN 0..count LOOP
        if sum = randomNum THEN
            close cur;
            exit;
        end if;
        FETCH cur INTO row;
        sum := sum+1;
    END LOOP;
 RETURN row;
END;
$row$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION randomAdress ()
RETURNS varchar(200) AS $row$
declare
    count int;
    randomNum int;
    row varchar(200);
    sum int;
    cur CURSOR FOR SELECT name FROM places;
BEGIN
    count := (SELECT COUNT(*) FROM places);
    randomNum :=  floor(random() * (count - 0) + 0);
    sum :=0;
    open cur;
    FOR i IN 0..count LOOP
        if sum = randomNum THEN
            close cur;
            exit;
        end if;
        FETCH cur INTO row;
        sum := sum+1;
    END LOOP;
 RETURN row;
END;
$row$ LANGUAGE plpgsql;




CREATE OR REPLACE FUNCTION addPerson () RETURNS void AS $$

declare
    sex INT;
    nameMenCount INT;
    nameMenRandom INT;
    name VARCHAR(200);
    surname VARCHAR(200);
    birthdate date;
    height int;
    weight int;
    adress varchar(200);
BEGIN
    sex := floor(random() *2 );
    IF sex = 1 THEN
        name := randomRowNameWomen();
        --raise notice 'Value: %', name;
        surname := randomSurname();
        surname := CONCAT(surname,'ová');
        height := floor(random() *55 + 150 );
        weight := floor(random() *150 + 30 );
    ELSE
        name := randomRowNameMen();
        surname := randomSurname();
        height := floor(random() *60 + 160 );
        weight := floor(random() *100 + 50 );
    END IF;
    birthdate =  date(now() - trunc(random()  * 365*100) * '1 day'::interval);
    adress := randomAdress();
    insert into people (sex,name,surname,birthdate,height,weight,adress) values (sex,name,surname,birthdate,height,weight,adress);
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION addPopulation (n int) RETURNS void AS $$
BEGIN
    FOR i IN 0..n-1 LOOP
        perform addPerson();
  END LOOP;
  perform createRelationships(n);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION createRelationships (n int) RETURNS void AS $$
BEGIN
    drop view if exists edges2;
    DROP TABLE IF EXISTS edges;
    CREATE TABlE edges (
        id serial primary key,
        a int NOT NULL,
        b int NOT NULL
    );
    insert into edges (a,b) select p1.id, p2.id from people as p1 cross join people as p2 where random() <0.005;
END;
$$ LANGUAGE plpgsql;

select addPopulation(100);

CREATE OR REPLACE FUNCTION numPeople ()
RETURNS int AS $count$
declare
    count int;
BEGIN
    count:=  count(*) from people;
 RETURN count;
END;
$count$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION mostPopulated1 ()
RETURNS void AS $$
declare
    count int;
BEGIN
    select name, count(*) from places group by name limit 10;
END;
$$ LANGUAGE plpgsql;


DROP TABLE IF EXISTS peopleRelationships;
CREATE TABlE peopleRelationships (
    first int,
    last int,
    path varchar(100),
    distance int
);

CREATE VIEW edges2 (a, b) AS (
  SELECT a, b FROM edges
  UNION ALL
  SELECT b, a FROM edges
);

CREATE OR REPLACE FUNCTION peopleRelationships (node int)
RETURNS void AS $$
begin
    WITH RECURSIVE transitive_closure(a, b, distance, path_string) AS
    ( SELECT a, b, 1 AS distance,
             a || '.' || b || '.' AS path_string,
             b AS direct_connection
        FROM edges2
       WHERE a = node -- set the starting node

       UNION ALL

      SELECT tc.a, e.b, tc.distance + 1,
             tc.path_string || e.b || '.' AS path_string,
             tc.direct_connection
        FROM edges2 AS e
        JOIN transitive_closure AS tc ON e.a = tc.b
       WHERE tc.path_string NOT LIKE '%' || e.b || '.%'
         AND tc.distance < 20
    )
    insert into peopleRelationships (first,last,path,distance) select a,b,path_string,distance FROM transitive_closure;
end;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION addRelationships (n int)
RETURNS void AS $$
begin
    FOR i IN 0..100 LOOP
        perform peopleRelationships(i);
    END LOOP;
end;
$$ LANGUAGE plpgsql;

select numPeople();
select addRelationships(100);
select * from peopleRelationships; -- all components
--select * from peopleRelationships order by distance desc limit 1; -- max relationship

select name, count(*) from places group by name limit 10;
