// cielom projektu je vytvorit pocet ludi a simulovat medzi nimi rozne vztahy na uzemi sk, takisto robit urcite statistiky tykajuce sa populacie
use population
db.population.drop()
db.createCollection("population");
var namesMen = ["Peter","Milan","Jakub","Juraj","Pavol","Ondrej","Oliver","Martin","Jozef","Ján","Andrej","Adam","Patrik"]; //nahodne muzske meno
var namesWomen = ["Katarína","Žaneta","Petra","Martina","Viera","Jaroslava","Zuzana","Lucia","Jarmila","Erika","Anna","Ľudmila"]; // nahodne zenske meno
var surnames = ["Slovák","Kučer","Košťál","Horvath","Petrík","Simčák","Hudák","Berger","Dolák","Rusnák","Bartík","Mlynarovič"]; //nahodne priezvisko
var nodes = [];

function addPerson(index){  // prida osobu do populacie
    var name,surname;
    sex = Math.floor((Math.random() * 2)); // nahodne pohlavie
    if(sex==0){  //ak je muz
        name = namesMen[Math.floor((Math.random() * namesMen.length))];
        surname = surnames[Math.floor((Math.random() * surnames.length))];
        height = Math.floor((Math.random() * 60)+160); // nahodna vyska sirka
        weight = Math.floor((Math.random() * 150)+50);
    }
    else{ //inac
        name = namesWomen[Math.floor((Math.random() * namesWomen.length))];
        surname = surnames[Math.floor((Math.random() * surnames.length))];
        surname +="ová";
        height = Math.floor((Math.random() * 55)+150);
        weight = Math.floor((Math.random() * 100)+30);
    }
    var c = db.addresses.count();
    var randomNumAddress = Math.floor((Math.random() * c)); //nahodna adresa

    db.population.insert({ //vlozi osobu do databazy
        index:index,
        sex:sex,
        name:name,
        surname:surname,
        birthdate: setDate(),
        address: db.addresses.find().limit(-1).skip(randomNumAddress).next(),
        height:height,
        weight:weight,
        family_ids:[],
        family_indexes:[]
    })
    nodes.push(index);
    return db.population.findOne({index:index});
}

function setDate(){ //nahodny rok narodenia
    var year = Math.floor((Math.random() *116)+1900);
    var mounth = Math.floor((Math.random() *12));
    var day = Math.floor((Math.random() *28));
    var d = new Date(year, mounth, day);
    return d.toDateString()
}

function addPopulation(num){  //prida pocet ludi do databazy a vytvori graf ich vztahov
    var gr = graph(num,0.005);
    for(var i=0;i<num;i++){
        addPerson(i);
    }
    for(var i=0;i<gr.edges.length;i++){
        var item = gr.edges[i];
        u1 = db.population.findOne({index:item.source})
        u2 = db.population.findOne({index:item.target})
        db.population.findOneAndUpdate({index:item.source},{$push:{family_ids:u2._id}})
        db.population.findOneAndUpdate({index:item.source},{$push:{family_indexes:u2.index}})
        db.population.findOneAndUpdate({index:item.target},{$push:{family_ids:u1._id}})
        db.population.findOneAndUpdate({index:item.target},{$push:{family_indexes:u1.index}})
    }
}

//funkcia ktora sa stara o vytvorenie vztahov v databaze
function graph(n, p) {
    var graph = { nodes: [], edges: [] },
    i, j;
    for (i = 0; i < n; i++) {
        graph.nodes.push({ label: 'node '+i });
        for (j = 0; j < i; j++) {
            if (Math.random() < p) {
                graph.edges.push({
                    source: i,
                    target: j
                });
            }
        }
    }
    return graph;
}

function personsRelationship(personIndex){ //zistuje s akyi ludmi ma jedna osoba vztah
    function dohlbky(person){
        visited.add(person._id.toString())
        for(var i=0;i<person.family_ids.length;i++){
            if(!visited.has(person.family_ids[i].toString())){
                var p2 = db.population.findOne({_id:person.family_ids[i]})
                dohlbky(p2);
            }
        }
    }

    var person = db.population.findOne({index:personIndex})
    var visited = new Set()
    dohlbky(person);
    return visited
}

function personsRelationshipToPath(personIndex,st,ar){ //zistuje s akyi ludmi ma jedna osoba vztah (v tvare string(cesty))
    function dohlbky(person,st,ar){
        visited.add(person.index.toString())
        for(var i=0;i<person.family_ids.length;i++){
            if(!visited.has(person.family_indexes[i].toString())){
                st+=","+person.family_indexes[i]
                var p2 = db.population.findOne({index:person.family_indexes[i]})
                ar.push(st);
                dohlbky(p2,st,ar);
            }
        }
    }

    var person = db.population.findOne({index:personIndex})
    var visited = new Set()
    st=""
    ar = []
    dohlbky(person,st,ar);
    return ar
}

function maxRelationshipSize(){ //kvazy najvacsi komponent
    var size;
    var maxSize=0;
    for(var i=0;i<nodes.length;i++){
        size = personsRelationship(nodes[i]).size;
        if(size>maxSize){
            maxSize = size;
        }
    }
    return maxSize;
}

function relationshipPeople(){ // vsetky komponenty su ulozene v db - size komponentu, ludia ktori tvoria tento komponent
    db.relationships.drop()
    db.createCollection("relationships");

    function dohlbky(person){
        visited.add(person._id.toString())
        set.add(person._id)
        for(var i=0;i<person.family_ids.length;i++){
            if(!visited.has(person.family_ids[i].toString())){
                var p2 = db.population.findOne({_id:person.family_ids[i]})
                dohlbky(p2);
            }
        }
    }
    var sum=0;
    var visited = new Set()
    for(var i=0;i<nodes.length;i++){
        var person = db.population.findOne({index:nodes[i]})
        var set = new Set()
        if(!visited.has(person._id.toString())){
            set.clear()
            dohlbky(person);
            db.relationships.insert({size:set.size,people:idsToPopulation(Array.from(set))})
            sum= sum+set.size
        }
    }
    return sum == nodes.length
}

function idsToPopulation(ar){ // skarede i objekty su zmenene na indexy (kvoli prehladonosti)
    var ar1 = [];
    var p;
    for(var i=0;i<ar.length;i++){
        p = db.population.findOne({_id:ar[i]});
        ar1.push(db.population.findOne({_id:ar[i]}));
    }
    return ar1;
}

function deadPerson(){ //vymaze jednotlivca z databazy
    var c = db.population.count();
    var randomPersonIndex = Math.floor((Math.random() * c));
    var deadPerson = db.population.findOne({index:randomPersonIndex});
    print("RIP "+ deadPerson.name+" "+deadPerson.surname+" "+deadPerson.birthdate+" "+ deadPerson.index)
    for(var i=0;i<deadPerson.family_ids.length;i++){
        db.population.findOneAndUpdate({_id:deadPerson.family_ids[i]},{$pull:{family_ids:deadPerson._id}})
        db.population.findOneAndUpdate({_id:deadPerson.family_ids[i]},{$pull:{family_indexes:deadPerson.index}})
    }
    nodes.splice(randomPersonIndex, 1);
    db.population.remove({index:randomPersonIndex});219
    print(randomPersonIndex);
}

function numPopulation(){       // zisti pocet populacie
    return db.population.count();
}

function heighestTen(){ // 10 najvyssich ludi
    return db.population.find().sort({"height":-1}).limit(10)
}

function mostPopulated(){ // naviac obyvana cast SK
    return db.population.aggregate([{$group : {_id : "$address", num : {$sum :1}}},{$sort:{num:-1}},{$limit:10}]);
}

function bornPerson(){ // clovek sa rodi
    var p = addPerson(nodes.length);
    print("born person: "+p.name+" "+p.surname);
}

//simulacia
function simulation(){
    while (true){
        sleep(2000);
        var rnd = Math.floor((Math.random() *2)+0);
        if(rnd==0){
            deadPerson();
            print("population " + numPopulation());
        }
        else{
            bornPerson();
            print("population " + numPopulation());
        }
    }
}


addPopulation(300);
// print(personsRelationship(0).size)
relationshipPeople();

// for(var i=0;i<100;i++){
//     print(i)
//     print(personsRelationshipToPath(i));
// }

//db.population.find()

//deadPerson()
//print(numPopulation());

//print(heighestTen());
//print(mostPopulated());
//print(numPopulation());s

//print(maxRelationshipSize(100));

//print(relationshipPeople());
//db.relationships.find().sort({size:-1})
//db.relationships.find({},{size:1,_id:0}).sort({size:-1})

// db.relationships.find({},{size:1,_id:0}).sort({size:-1})
// simulation();
// db.relationships.find({},{size:1,_id:0}).sort({size:-1})
